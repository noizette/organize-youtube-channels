<?php 
namespace Yt;

function dump($v, $t = 'p', $s = ''){
	switch($t)
	{
		case 'c':
			echo '<script>console.log('.str_replace('\\u0000', '', json_encode((array)$v)).')</script>';	
			break;
		default:
		case 'p':
			echo "<br>$s<br>";
			highlight_string("<?php\n" . print_r($v, true) . ";?>\n");
			break;
		case 'e':
			highlight_string("<?php\n" . var_export($v, true) . ";?>\n");
			break;
	}
	// echo '<script>document.getElementsByTagName("code")[0].getElementsByTagName("span")[1].remove() ;document.getElementsByTagName("code")[0].getElementsByTagName("span")[document.getElementsByTagName("code")[0].getElementsByTagName("span").length - 1].remove() ; </script>';
}

function simpleCharset(string $s, $spaces = true)
{
	return preg_replace('([^0-9A-Za-z-_'.($spaces?' ':'').'])', '', $s);
}

function html($string) {
    return htmlspecialchars($string,  ENT_QUOTES | ENT_XHTML, 'UTF-8');
}

function html_dec($string) {
    return htmlspecialchars_decode($string,  ENT_QUOTES | ENT_XHTML);
}

if( !function_exists('array_key_last') ) {
    function array_key_last(array $array) {
        if( is_array($array) && !empty($array) ) return key(array_slice($array, -1, 1, true));
    }
}