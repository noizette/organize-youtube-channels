<?php

namespace Yt;

use Yt\Gallery;

class Api {

	private $session = NULL;
	protected $apiUrl = '';
	protected $key = '';

    static protected $_instance = null;

    static public function getInstance()
    {
        return self::$_instance ?: self::$_instance = new Api();
	}
	
	public function __construct($url = '')
	{
		$db = Gallery::getInstance();
		if (empty($url))
		{
			$this->apiUrl = $db->getInvidiousSettings()['url'].'/api/v1/';
		}
		else
		{
			$this->apiUrl = $url;
		}
	}
	
	public function setUrl($url = '')
	{
		if (empty($url))
		{
			return false;
		}

		$this->apiUrl = $url;
	}

	public function setKey($key = '')
	{
		if (empty($key))
		{
			return false;
		}

		$this->key = $key;
	}
	
	private function request($opt = [], $url = '')
	{
		$this->session = curl_init($this->apiUrl . $url);

		$opt = ([ CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTPHEADER => [ 'Content-Type: application/json',
									],
				] + $opt);
		curl_setopt_array($this->session, $opt);

		$r = curl_exec($this->session);

		curl_close($this->session);
		
		return $r;
	}

	public function get($content, $params = [])
	{
		// Éventuels paramètres passés
		$p = '';
		if (!empty($params))
		{
			$p = '?';
			foreach ($params as $param => $value)
			{
				$p .= $param .'='. $value .'&';
			}
		}

		if(!empty($this->key))
		{
			if (empty($p))
			{
				$p = '?';
			}
			$p .= 'key='.$this->key;
		}

		// Effectue la requete url + paramètres
		$r = json_decode($this->request([], $content.$p));

		// Vérifie qu'il y ait quelque chose dans la réponse
		if (empty((array)$r))
		{
			throw new \Exception('API error: API returned empty response (you may retry, or parameters may be wrong?).');
		}
		// Vérifie que la réponse est JSON-valid
		if ($r === NULL)
		{
			throw new \Exception('API error: Could not decode API response.');
		}
		// Vérifie si la réponse n'est pas une erreur
		if (isset($r->error))
		{
			throw new \Exception(print_r($r->error, true));
		}
		if (isset($r->code))
		{
			throw new \Exception(print_r('API error: '. $r->code .' - '. $r->message, true));
		}

		return $r;
	}
	
}