<?php

namespace Yt;

class Form {

	// private $datas = [];
	
	public function __construct($datas = []){
		// $this->datas = $datas;
	}
	
	private function input($type, $name, $label, $value, $isRequired, $class = '', $checked = false){
		$isRequired = $isRequired ? "required":'';
		$class = empty($class) ? '':'class="'.$class.'"'; 
		$checked = $checked ? 'checked':''; 


		$input = "<input $isRequired type='$type' name='$name' id='i-$name' value='$value' $checked class='form-control'>";

		return "<div class='form-group'><label $class for='i-$name'>$label</label>$input</div>";
	}

	public function number($name, $label, $value, $isRequired = true){
		return $this->input('number', $name, $label, $value, $isRequired);
	}

	public function text($name, $label, $value = '', $isRequired = true, $class = ''){
		return $this->input('text', $name, $label, $value, $isRequired, $class);
	}

	public function checkbox($name, $label, $checked = false, $isRequired = true, $class = ''){
		return $this->input('checkbox', $name, $label, '', $isRequired, $class, $checked);
	}

	public function submit(string $label, string $name = ''){
		return '<input type="submit" value="'.$label.'" name="'.$name.'" class="btn"/>';
	}
}