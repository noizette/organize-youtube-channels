<?php
namespace Yt;
require '../inc/_inc.php';

$tpl['title'] = "Erreur" . (isset($tpl['err'])? ' '.$tpl['err']:'');

?>

<div class="container">
    <h2><?= $tpl['title'] ?></h2>
    <p>
        Pas de soucis, ça arrive à tout le monde de faire des erreurs !
        <a href="./">Clique ici pour retourner à la page d'accueil et retrouver tes pas</a> !
    </p>
    <p><?= $tpl['err_desc'] ?? ''?></p>
</div>