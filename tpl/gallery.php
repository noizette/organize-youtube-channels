<?php
namespace Yt;
require '../inc/_inc.php';

$tpl['title'] = $db->selCatById($tpl['g'])->getName();

?>

<?php 
try
{

	if (!empty($tpl['tag']))
	{
		$db->formatElementsTagged($tpl['tag']);
	}
	else
	{
		$db->formatedList();
	}

	foreach($db->getErrors() as $err)
	{
		$tpl['err'][] = $err;
	}
	

}
catch (\Exception $e)
{
    $tpl['err'][] = $e->getMessage();
}