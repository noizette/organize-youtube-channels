<?php
namespace Yt;

use Exception;

require '../inc/_inc.php';

$tpl['title'] = "Editeur";

?>

<?php
/********  TRAITEMENT *********/
try
{
	do
	{
		if(!empty($_POST))
		{
			$formNames = ['s_inv', 's_addcat', 's_addchan', 's_bulkadd', 's_main', 's_pp', 's_rm_tags', 's_xml'];
			$formIdentifier = [];

			foreach ($formNames as $fname)
			{
				if(array_key_exists($fname, $_POST))
				{
					$formIdentifier = $fname;
					break;
				}
			}

			// Select category if correctly transmitted
			if ($formIdentifier != 's_inv' && $formIdentifier != 's_rm_tags')
			{
				if(!isset($_POST['cat']))
				{
					$tpl['err'][] = 'POST datas received but no \'cat\' var found.';
					break;
				}
				else
				{
					$db->selCatById($_POST['cat']);
				}			
			}

			switch($formIdentifier)
			{
				case 's_inv' :
					$en = isset($_POST['inv_enable']) ? true:false;
					$db->setInvidiousSettings($en, (string) $_POST['inv_url']);
					break;
				case 's_addcat':
					if(!$db->isNameFree($_POST['cat-name']))
					{
						$tpl['err'][] = 'Name '.$_POST['cat-name'].' is already taken.';
						break;
					}
					$order = is_numeric($_POST['cat-order'])?$_POST['cat-order']:'0';
						$db->addCat((int) $order, simpleCharset($_POST['cat-id'], 0), html($_POST['cat-name']), $_POST['pp']??'');
						break;
				case 's_addchan':
					if(!$db->isNameFree($_POST['chan-name']))
					{
						$tpl['err'][] = 'Name '.$_POST['chan-name'].' is already taken.';
						break;
					}
					$order = is_numeric($_POST['chan-order'])?$_POST['chan-order']:($db->selCatById($tpl['g'])->getMaxOrder() + 1);
					$db->selCatById($_POST['cat'])
						->addChan((int) $order, $_POST['chan-link'], html($_POST['chan-name']), $_POST['pp']??'');
						
					$tpl['success'][] = 'Channel added: '. $db->getSelected()['name'];
					break;
				case 's_bulkadd':
					if (empty($_POST['bulkadd']))
					{
						throw new Exception('Empty bulkadd field');
					}

					$text = $_POST['bulkadd'];
					$list = explode(PHP_EOL, $text);
					foreach ($list as $link)
					{
						// Skip empty lines
						if (empty(trim($link)))
						{
							continue;
						}

						$order = $db->selCatById($tpl['g'])->getMaxOrder() + 1;
						$db->selCatById($_POST['cat'])
						->addChan($order, trim($link));
						
						$tpl['success'][] = 'Channel added: '. $db->getSelected()['name'];
					}
					break;
				case 's_main':
					$i = 0;
					foreach($db->getOrderedList() as $v)
					{
						if(isset($_POST[$i]['rm']))
						{
							$db->selByName($v['name'], $_POST['cat'])->rmSelected()->listNames();
							$tpl['success'][] = $v['name'].' was successfuly deleted.';
							$i++;
							continue;
						}
						
						$selected = false;
						if($v['name'] != $_POST[$i]['name'])
						{
							if(!$db->isNameFree($_POST[$i]['name']))
							{
								$tpl['err'][] = $_POST['cat'];
								$tpl['err'][] = 'Line '.__LINE__.", \$i: $i -- ".$_POST[$i]['name'].' is already taken.';
								break;
							}
							$db->selByName($v['name'], $_POST['cat'])->setName(html($_POST[$i]['name']))->listNames();
							$selected = true;
						}
							
						if($v['pp'] != $_POST[$i]['pp'])
						{
							$selected?:$db->selByName($v['name'], $_POST['cat']);
							$db->setPP($_POST[$i]['pp']);
							$selected = true;
						}
						
						if($v['order'] != $_POST[$i]['order'])
						{
							$selected?:$db->selByName($v['name'], $_POST['cat']);
							is_numeric($_POST[$i]['order']) ?
							$db->setOrder((int)html($_POST[$i]['order'])):
							$tpl['err'][] = 'Order is not a number: '.$_POST[$i]['order'];
							$selected = true;
						}
						
						if($v['tags'] != $_POST[$i]['tags'])
						{
							$selected?:$db->selByName($v['name'], $_POST['cat']);
							if (!preg_match('/^([0-9A-Za-z-_,]*)$/', $_POST['cat']))
							{
								$tpl['err'][] = $i.': Tags must be [0-9A-Za-z-_]. Other chars removed.';
							}
							$db->setTags($_POST[$i]['tags']);
							$selected = true;
						}
						
						if($v['isCat'])
						{
							$selected?:$db->selByName($v['name'], $_POST['cat']);
							$v['id'] != $_POST[$i]['id'] ?
							$db->setId(html($_POST[$i]['id'])):'';
						}
						else
						{
							$selected?:$db->selByName($v['name'], $_POST['cat']);
							($v['link'] != $_POST[$i]['link'] ?
								$db->setLink($_POST[$i]['link']):'');
						}
											
						$i++;
					}
					$tpl['success'][] = 'Updated channels';
					break;
				case 's_pp':
					foreach($db->getSelected()->chan as $chan)
					{
						$chan['pp'] = $db->getChanThumb((string)$chan['link'])?:$chan['pp'];
					}
					break;
				case 's_rm_tags':
					if(!$r = $db->removeUnusedTags())
					{
						$tpl['success'][] = 'No tags removed.';
					}
					else
					{
						$tpl['success'][] = 'Tags removed: '.implode(', ', $r).'.';
					}

					break;
				case 's_xml':
					if(!$db->loadXMLString($_POST['xml_content']))
					{
						$tpl['err'][] = 'Error while processing received XML.';
						$tpl['post_xml'] = $_POST['xml_content'];
						break;
					}
					$tpl['success'][] = 'Valid XML processed.';
					break;
			}

			$db->saveXML();

		}
	} while (0);
}
catch (Exception $e)
{
    $tpl['err'][] = $e->getMessage();
}


/******** AFFICHAGE *********/

?>
	<h3><b>Editor</b> - <a href="
	<?php
	echo isset($_GET['xml'])?
	'?"> edit with GUI':
	'?xml"> edit as XML';
	?></a></h3>
	<p><i>Try not use special chars, like &amp;. Things like &apos;, &lt; or &gt; are a bit squeaky in forms but doesn't break anything.</i></p>
	<?php

$f = new Form();

/******** XML EDITOR *******/

if(isset($_GET['xml']))
{
	?>
	<em>Due to problems with textarea and escaped characters I don't, you need to copy the current content from before, and html-escape chars in attributes values.</em>
	<form action="editor?xml" method="post">
		<input type="hidden" name="cat" value=""/>
		<textarea class="form-control" rows="30" name="xml_content"><?= 
			$tpl['post_xml']??'Copy content below :(';
			?></textarea>
		<?= $f->submit('Send', 's_xml'); ?>
	</form>
	<?php
	echo '<pre>'.highlight_string(print_r($db->selRoot()->asXML(), true), true).'</pre>';
}
/************************/
else
{	?>	

	<h3>Browse categories:</h3><?php
	$db->selCatById($tpl['g'])->formatAriane(['editor' => ''])->formatedList(false, true);?>

	<h3>Miscellaneous:</h3>

	<form action="editor?g=<?= $tpl['g']?>" method="post">
		<input type="hidden" name="cat" value="<?= $db->getId()?>"/>
		<label for='invi' class='lbl-toggle btn'>Invidious settings...</label>
		<input id='invi' class='toggle' type='checkbox'/>
		<div class="collapsible-content">
			<?php
			$inv = $db->getInvidiousSettings();
			echo $f->checkbox('inv_enable', 'Enable Invidious', (int)$inv['enabled']?true:false, false, 'invidious');
			echo $f->text('inv_url', 'Instance URL', $inv['url'], false, 'invidious');
			echo $f->submit('Save', 's_inv');
			?>
		</div>
	</form>

	<form action="editor?g=<?= $tpl['g']?>" method="post">
		<input type="hidden" name="cat" value="<?= $db->getId()?>"/>
		<label for='addcat' class='lbl-toggle btn'>Add category...</label>
		<input id='addcat' class='toggle' type='checkbox'/>
		<div class="collapsible-content">
			<?php
			echo $f->text('cat-name', 'Category name', '');
			echo $f->text('cat-id', 'Category id', '');
			echo $f->number('cat-order', 'Category order', '');
			echo "<br>";
			echo $f->submit('Add category!', 's_addcat');
			?>
		</div>
	</form>

	<form action="editor?g=<?= $tpl['g']?>" method="post">
		<input type="hidden" name="cat" value="<?= $db->getId()?>"/>
		<label for='addchan' class='lbl-toggle btn'>Add channel...</label>
		<input id='addchan' class='toggle' type='checkbox'/>
		<div class="collapsible-content">
			<?php
			echo $f->text('chan-name', 'Channel name <i>(optional)</i>', '', false);
			echo $f->text('chan-link', 'Channel link', '');
			echo $f->number('chan-order', 'Channel order <i>(optional)</i>', '', false);
			echo "<br>";
			echo $f->submit('Add channel!', 's_addchan');
			?>
		</div>
	</form>

	<form action="editor?g=<?= $tpl['g']?>" method="post">
		<input type="hidden" name="cat" value="<?= $db->getId()?>"/>
		<label for='bulkadd' class='lbl-toggle btn'>Bulk add channels...</label>
		<input id='bulkadd' class='toggle' type='checkbox'/>
		<div class="collapsible-content">
			<textarea class="form-control" rows="15" name="bulkadd"></textarea>
			<?php
			echo "<br>";
			echo $f->submit('Add channels!', 's_bulkadd');
			?>
		</div>
	</form>

	<form action="editor?g=<?= $tpl['g']?>" method="post">
		<input type="hidden" name="cat" value="<?= $db->getId()?>"/>
		<?= $f->submit('Update PPs', 's_pp');?>
	</form> 

	<form action="editor?g=<?= $tpl['g']?>" method="post">
		<?= $f->submit('Remove unused tags', 's_rm_tags');?>
	</form> 

	<h3>Edit elements:</h3>
	<form action="editor?g=<?= $tpl['g']?>" method="post">
		<input type="hidden" name="cat" value="<?= $db->getId()?>"/>
	<?php
	$i = 0;
	foreach($db->selCatById($tpl['g'])->getOrderedList() as $v)
	{
		echo "<div class='thumbnails'>";
		
		echo "<img src='".$v['pp']."'>";
		
		echo $f->text($i.'[name]', 'Name', html($v['name']), false);
		
		echo $f->number($i.'[order]', 'Order', $v['order']);
		
		echo $v['isCat'] ?
		$f->text($i.'[id]', 'Id', $v['id']):
		$f->text($i.'[link]', 'Link', $v['link']);

		echo $f->text($i.'[pp]', 'Picture', $v['pp'], false);
		
		echo $f->text($i.'[tags]', 'Tags', $v['tags'], false);
		
		echo $f->checkbox($i.'[rm]', '<span>Remove</span>', '', false, 'remove');
		
		echo "</div>";

		$i++;
	}

	echo "<br/><br/>";
	echo $f->submit('Submit changes', 's_main');

	echo '</form>';
}

foreach($db->getErrors() as $err)
{
	$tpl['err'][] = $err;
}
