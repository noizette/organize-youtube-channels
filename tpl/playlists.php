<?php
namespace Yt;
require '../inc/_inc.php';

$f = new Form();

$tpl['title'] = 'Suivi des playlists';

?>

<?php 

$url = $db->watch_url;

try
{
    $api = Api::getInstance();
    $api->setUrl('https://youtube.googleapis.com/youtube/v3/');
    $api->setKey(API_KEY);
    
    // **** Traitement POST ****
    
	if(!empty($_POST))
	{
        if (isset($_POST['s_updatepl']) && isset($_POST['plId']))
        {
            if (!$db->checkPlaylist($_POST['plId']))
            {
                $tpl['success'][] = 'No updates';
            }
            else
            {
                $tpl['success'][] = 'Successfuly updated';
            }
            $db->saveXML();
        }

        if (isset($_POST['s_addpl']) && isset($_POST['url']))
        {
            $id = $db->getPlaylistIdFromUrl($_POST['url']);
            $db->checkPlaylist($id);
            $db->saveXML();
            $tpl['id'] = $id; // Redirect ?
        }
    }


    // **** Affichage ****

    echo '<h3>Saved playlists:</h3>';

    if ($db->selPlaylists())
    {
        echo '<ul>';
        foreach ($db->getSelected()->children() as $pl)
        {
            echo '<li><a href="?id='.$pl['playlistId'].'">';
            echo $pl['title'] . ' - '. $pl['author'];
            echo '</a></li>';
        }
        echo '</ul>';
    }
    else
    {
        echo 'No playlist tracked at the moment.';
    }   
    
    ?>
    <br>
	<form action="" method="post">
		<label for='addchan' class='lbl-toggle'>Track a playlist...</label>
		<input id='addchan' class='toggle' type='checkbox'/>
		<div class="collapsible-content">
			<?php
			echo $f->text('url', 'URL:', '');
			echo "<br>";
			echo $f->submit('Add playlist!', 's_addpl');
			?>
		</div>
	</form>
    <br>
    <?php
    
    
    // **** Display selected playlist ****

    if (!empty($tpl['id']))
    {
        $id = $tpl['id'];

        $db->selPlaylist($id);
        $info = $db->getPlaylistInfo();
        $pl = $db->buildPlaylist();
        
        echo '<h2><a href="'.$url.'/playlist?list='.$id.'">'.$info['title'].'</a> - '.$info['author'].'</h2>';

        ?>      
        <form action="" method="post">
            <input type="hidden" name="plId" value="<?= $id ?>"/>
            <?= $f->submit('Update this playlist', 's_updatepl');?>
        </form>
        <br>
        <?php

        foreach ($pl as $id => $vid)
        {
            echo '<a href="'.$url.'/watch?v='.$id.'">'. $vid['title'] .'</a> - '.$vid['author'].'<br>';
        }

        
        echo '<h3>Historic</h3>';
        
        $changes = $db->getSelected()->changes;
        echo '<ul class="historic">';
        foreach ($changes as $change)
        {
            echo '<li>';

            echo strftime('<em>%a %d %b %Y - %H:%M </em>', (int) $change['time']);
            echo '<br>';
            foreach ($change as $id => $video)
            {
                echo '<span class="historic '.$video['status'].'">';
                echo $video['status'].': ';
                echo $video['videoId'].' - ';
                echo $video['title'].' - ';
                echo $video['author'];
                echo '</span><br>';
            }
            echo '</li>';
        }
        echo '</ul>';
    }

}
catch (\Exception $e)
{
    $tpl['err'][] = $e->getMessage();
}
