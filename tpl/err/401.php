<?php
$tpl['err'] = 401;
$tpl['err_desc'] = 'Vous tentez d\'accéder à un contenu non-autorisé (on a pas appelé la police tkt).<br>
Unauthorized<br>
This server could not verify that you are authorized to access the document requested. Either you supplied the wrong credentials (e.g., bad password), or your browser doesn\'t understand how to supply the credentials required.';
http_response_code(401);
require '../tpl/error.php';